#include "TestEngine.h"
#include "Engine/Simulation.h"
#include "Platform/Win32/WinEntry.h"

class TestEngine : public Thunder::Simulation
{
	//constructor
	public: 
	TestEngine() { }

	//deconstructor
	public:
	~TestEngine(){ }

	public:
	VOID SetGameSettings();
	VOID Init() { }

	//Game loop
	VOID Update() { }
};

ENTRYAPP(TestEngine)

VOID TestEngine::SetGameSettings()
{
	PerGameSettings::SetGameName(IDS_PERGAMENAME);
	PerGameSettings::SetShortName(IDS_SHORTNAME);
	PerGameSettings::SetMainIcon(IDI_MAINICON);
	PerGameSettings::SetSplashUrl(IDS_SPLASHURL);
}