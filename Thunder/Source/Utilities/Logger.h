
class THUNDER_API Logger
{
	private:
		static Logger* inst;

	public:
		static Logger* GetInstance() { return inst; }

	public:
		Logger();
		~Logger();

	static VOID PrintLog(const WCHAR* fmt, ...);
	static std::wstring LogDirectory();
	static std::wstring LogFile();
	static VOID PrintDebugSeparator();
	static BOOL IsMTailRunning();
	static VOID StartMTail();
};