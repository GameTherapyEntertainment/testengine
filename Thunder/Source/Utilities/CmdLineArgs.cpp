#include "Thunder.h"
#include "CmdLineArgs.h"
#include <algorithm>

VOID CmdLineArgs::ReadArgs()
{
	int argc = 0;
	LPWSTR* argv = CommandLineToArgvW(GetCommandLineW(), &argc);

	//start from 1 cause the first command line is always the url of the build directory
	for (int i = 1; i < argc; i++)
	{
		std::wstring key = argv[i];

		bool isFirstCharHyphen = key[0] == '-';
		if (isFirstCharHyphen)
		{
			//erase hyphen
			key.erase(0, 1);
			std::transform(key.begin(), key.end(), key.begin(), ::tolower);
			ReadArgument(key.c_str());
		}
	}
}

VOID CmdLineArgs::ReadArgument(const WCHAR* arg)
{
	//wcscmp = string compare
	if(wcscmp(arg, L"mtail") == 0) Logger::StartMTail();
	if (wcscmp(arg, L"debug") == 0) Engine::SetMode(Engine::EngineMode::DEBUG);
	if (wcscmp(arg, L"editor") == 0) Engine::SetMode(Engine::EngineMode::EDITOR);
	if (wcscmp(arg, L"server") == 0) Engine::SetMode(Engine::EngineMode::SERVER);
}
