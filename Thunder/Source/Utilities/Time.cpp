#include "Thunder.h"
#include "Helpers.h"
#include <ctime>
#include <sstream>
#include <iomanip>

//Get Time in format '00:00:00'
std::wstring THUNDER_API Time::GetTime(bool stripped)
{
	time_t now = time(0);
	tm ltm;
	localtime_s(&ltm, &now);
	std::wstringstream stream;
	stream << std::put_time(&ltm, L"%T");

	std::wstring timeString = stream.str();

	if (stripped)
	{
		/*std::wstring chars = L":";
		for (WCHAR c : chars)
		{
			timeString.erase(std::remove(timeString.begin(), timeString.end(), c), timeString.end());
		}*/
		return Helpers::GetInstance()->StripTimeString(L":", timeString);
	}
	return timeString;
}

//Get date time in format '00/00/00 00:00:00'
std::wstring THUNDER_API Time::GetDate(bool stripped)
{
	time_t now = time(0);
	tm ltm;
	localtime_s(&ltm, &now);
	std::wstringstream stream;
	stream << std::put_time(&ltm, L"%d/%m/%y");

	std::wstring timeString = stream.str();

	if (stripped)
	{
		/*std::wstring chars = L"/";
		for (WCHAR c : chars)
		{
			timeString.erase(std::remove(timeString.begin(), timeString.end(), c), timeString.end());
		}*/
		return Helpers::GetInstance()->StripTimeString(L"/", timeString);
	}
	return timeString;
}

std::wstring THUNDER_API Time::GetDateTimeString(bool stripped)
{
	std::wstring timeString = GetDate(stripped) + L" " + GetTime(stripped);

	if (stripped)
	{
		/*std::wstring chars = L" ";
		for (WCHAR c : chars)
		{
			timeString.erase(std::remove(timeString.begin(), timeString.end(), c), timeString.end());
		}*/
		return Helpers::GetInstance()->StripTimeString(L" ", timeString);
	}
	return timeString;
}