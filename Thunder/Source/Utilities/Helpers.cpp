#include "Thunder.h"
#include "Helpers.h"

Helpers* Helpers::inst;

Helpers::Helpers()
{
	inst = this;
}

Helpers::~Helpers()
{
}

//Time string in format = 00000000
std::wstring Helpers::StripTimeString(std::wstring chars, std::wstring timeString)
{
	for (WCHAR c : chars)
	{
		timeString.erase(std::remove(timeString.begin(), timeString.end(), c), timeString.end());
	}
	return timeString;
}
