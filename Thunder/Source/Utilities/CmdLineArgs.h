#pragma once

namespace CmdLineArgs
{
	enum Args
	{
		debug, server, editor 
	};
	VOID THUNDER_API ReadArgs();
	VOID THUNDER_API ReadArgument(CONST WCHAR* arg);
}