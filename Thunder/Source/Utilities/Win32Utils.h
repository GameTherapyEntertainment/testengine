#pragma once

#define DEFAULT_WIDTH 800
#define DEFAULT_HEIGHT 600

namespace Win32
{
	enum  WindowType : DWORD
	{
		STATIC = WS_OVERLAPPED,
		RESIZABLE = WS_SIZEBOX,
		POPUP = WS_POPUP
	};



	namespace Utils
	{
		bool THUNDER_API AddBitmap(const WCHAR* szFileName, HDC hWinDC, int x = 0, int y = 0);

		inline bool THUNDER_API IsWindowFullscreen(HWND hWnd)
		{
			WINDOWPLACEMENT placement;
			GetWindowPlacement(hWnd, &placement);

			if (placement.showCmd == SW_MAXIMIZE)
				return TRUE;
			return FALSE;
		}

		inline void THUNDER_API MaximizeWindow(HWND hwnd)
		{
			WINDOWPLACEMENT wPos;
			GetWindowPlacement(hwnd, &wPos);
			if (wPos.showCmd == SW_MAXIMIZE) ShowWindow(hwnd, SW_NORMAL);
			else  ShowWindow(hwnd, SW_MAXIMIZE);
		}
	}
}