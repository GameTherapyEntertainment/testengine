#pragma once
#include "Thunder.h"

class Helpers
{
	public:
	Helpers();
	~Helpers();

	public:
	static Helpers* GetInstance()
	{
		return inst;
	}
	std::wstring StripTimeString(std::wstring chars, std::wstring timeString);

	private:
	static Helpers* inst;
};