
namespace Time
{
	std::wstring THUNDER_API GetTime(bool stripped = false);
	std::wstring THUNDER_API GetDate(bool stripped = false);
	std::wstring THUNDER_API GetDateTimeString(bool stripped = false);
}