#pragma once

#include <string>
#include <fstream>
#include <list>

using std::list, std::wstring;

#include "Core/Definitions.h"

#include "Engine/OEngine.h"

#include "Utilities/Time.h"
#include "Utilities/Logger.h"
#include "Core/PerGameSettings.h"

#ifdef  WIN32
	#include "Utilities/Win32Utils.h"
	#include "Platform/Win32/SubObject.h"
	#include "Platform/Win32/Caption.h"
	#include "Platform/Win32/Window.h"
	#include "Platform/Win32/IApplication.h"
#endif //  WIN32
