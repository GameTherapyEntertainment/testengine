#pragma once


#ifdef  BUILD_DLL
#define THUNDER_API _declspec(dllexport)
#else
#define THUNDER_API _declspec(dllimport)
#endif

#define HInstance() GetModuleHandle(NULL)
#define MAX_STR_SIZE 256