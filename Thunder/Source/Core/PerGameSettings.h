
class THUNDER_API PerGameSettings
{
	private:
		static PerGameSettings* inst;
		static PerGameSettings* GetInstance()
		{
			return inst;
		}

	public:
		PerGameSettings();
		~PerGameSettings();

	private:
	TCHAR m_GameName[MAX_STR_SIZE];
	TCHAR m_ShortName[MAX_STR_SIZE];
	HICON m_MainIcon;
	TCHAR m_BootTime[MAX_STR_SIZE];
	WCHAR m_SplashUrl[MAX_STR_SIZE];

	public:
	static TCHAR* SplashUrl()
	{
		return inst->m_SplashUrl;
	}
	static void SetSplashUrl(INT id)
	{
		LoadString(HInstance(), id, inst->m_SplashUrl, MAX_STR_SIZE);
	}


	static TCHAR* GameName()
	{
		return inst->m_GameName;
	}
	static VOID SetGameName(UINT id)
	{
		LoadString(HInstance(), id, inst->m_GameName, MAX_STR_SIZE);
	}

	static TCHAR* ShortName()
	{
		return inst->m_ShortName;
	}
	static VOID SetShortName(UINT id)
	{
		LoadString(HInstance(), id, inst->m_ShortName, MAX_STR_SIZE);
	}

	static TCHAR* BootTime()
	{
		return inst->m_BootTime;
	}
	static VOID SetBootTime(UINT id)
	{
		LoadString(HInstance(), id, inst->m_BootTime, MAX_STR_SIZE);
	}

	static HICON MainIcon()
	{
		return inst->m_MainIcon;
	}
	static VOID SetMainIcon(UINT id)
	{
		LoadIcon(HInstance(), MAKEINTRESOURCE(id));
	}
};
