#include "Thunder.h"

PerGameSettings* PerGameSettings::inst;

PerGameSettings::PerGameSettings()
{
	inst = this;

	wcscpy_s(inst->m_GameName, L"default");
	wcscpy_s(inst->m_ShortName, L"default");
	wcscpy_s(inst->m_BootTime, Time::GetDateTimeString(TRUE).c_str());
	wcscpy_s(inst->m_SplashUrl, L"..\\Thunder\\Content\\Images\\Gears 5.bmp");
}

PerGameSettings::~PerGameSettings()
{
}
