#pragma once

namespace Thunder
{
	class THUNDER_API Simulation : public Win32::IApplication, public Win32::Window
	{
		public:
		Simulation();
		~Simulation();

		virtual void PreInit() override;
		LRESULT MessageHandler(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) override;
	};
}