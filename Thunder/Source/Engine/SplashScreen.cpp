#include "Thunder.h"
#include "SplashScreen.h"
#include "Utilities/Win32Utils.h"

namespace SpashScreen
{
	#define WM_OUTPUTMESSAGE (WM_USER + 0x0001)

	SplashWindow* m_SplashWindow;

	VOID Open()
	{
		if (m_SplashWindow != nullptr)
		{
			return;
		}
		m_SplashWindow = new SplashWindow();
	}

	VOID Close()
	{
		return VOID THUNDER_API();
	}

	VOID AddMessage(const WCHAR* message)
	{
		PostMessage(m_SplashWindow->GetHandle(), WM_OUTPUTMESSAGE, (WPARAM)message, 0);
	}
}


SplashWindow::SplashWindow() : Win32::Window(L"Splash Screen", NULL, Win32::WindowType::POPUP)
{
	wcscpy_s(m_outputMessage, L"Splash screen starting...");
	Win32::Window::RegisterObject();
	SetSize(500, 600);
	Win32::Window::Init();
}

SplashWindow::~SplashWindow()
{
}

LRESULT SplashWindow::MessageHandler(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		case WM_PAINT:
		{
			HBITMAP hbitmap;
			HDC hdc, hmemdc;
			PAINTSTRUCT ps;

			hdc = BeginPaint(hwnd, &ps);

			Win32::Utils::AddBitmap(PerGameSettings::SplashUrl(), hdc);

			SetBkMode(hdc, TRANSPARENT);
			SetTextColor(hdc, RGB(255, 255, 255));

			SIZE size = GetSize();

			if (Engine::GetMode() != Engine::EngineMode::RELEASE) {

				std::wstring engineModeText = Engine::EngineModeToString() + L" Mode";
				SetTextAlign(hdc, TA_RIGHT);
				TextOut(hdc, size.cx - 15, 15, engineModeText.c_str(), wcslen(engineModeText.c_str()));
			}

			SetTextAlign(hdc, TA_CENTER);

			TextOut(hdc, size.cx / 2, size.cy - 30, m_outputMessage, wcslen(m_outputMessage));
			EndPaint(hwnd, &ps);
		}
			break;
			case WM_OUTPUTMESSAGE:
			{
				WCHAR* msg = (WCHAR*)wParam;
				wcscpy_s(m_outputMessage, msg);
				RedrawWindow();
				return 0;
			}
	}

	return Window::MessageHandler(hwnd, message, wParam, lParam);
}
