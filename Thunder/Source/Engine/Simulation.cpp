#include "Thunder.h"
#include "Simulation.h"
#include "SplashScreen.h"


namespace Thunder
{
	Simulation::Simulation()
		: Win32::Window(L"Main Application", NULL)
	{
	}

	Simulation::~Simulation()
	{
	}


	void Simulation::PreInit()
	{
		Logger::PrintDebugSeparator();
		Logger::PrintLog(L"Application starting...\n");
		Logger::PrintLog(L"%s\n", L"Game NAme");
		Logger::PrintLog(L"Boot time: %s\n", Time::GetDateTimeString().c_str());
		Logger::PrintLog(L"Engine mode: %s\n", Engine::EngineModeToString().c_str());
		Logger::PrintDebugSeparator();

		SpashScreen::Open();

		Win32::Window::RegisterObject();
		Win32::Window::Init();
	}

	LRESULT Simulation::MessageHandler(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
	{
		switch (message)
		{

		}

		return Window::MessageHandler(hwnd, message, wParam, lParam);
	}
}
