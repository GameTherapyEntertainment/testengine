#pragma once
#include "Platform/Win32/Window.h"

namespace SpashScreen
{
	VOID THUNDER_API Open();
	VOID THUNDER_API Close();
	VOID THUNDER_API AddMessage(CONST WCHAR* message);
}

class THUNDER_API SplashWindow : public Win32::Window
{
	public:
	SplashWindow();
	~SplashWindow();

	virtual			LRESULT				MessageHandler(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) override;

	private:
	WCHAR m_outputMessage[MAX_STR_SIZE];
};