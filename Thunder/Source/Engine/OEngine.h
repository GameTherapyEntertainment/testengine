#pragma once

class  THUNDER_API OEngine;
namespace Engine
{
	enum EngineMode : INT
	{
		NONE, DEBUG, RELEASE, EDITOR, SERVER
	};

	extern OEngine g_ThunderEngine;

	VOID THUNDER_API SetMode(EngineMode mode);
	EngineMode THUNDER_API GetMode();
	std::wstring THUNDER_API EngineModeToString();
}

using namespace Engine;
class THUNDER_API OEngine
{
	public:
	OEngine();
	~OEngine();
	EngineMode GetEngineMode();
	VOID SetEngineMode(EngineMode mode);

	private:
	EngineMode m_EngineMode;
};
