#include "Thunder.h"

#include "IApplication.h"
#include "Utilities/CmdLineArgs.h"

#pragma region ProjectStages
//stage 1: https://www.youtube.com/watch?v=2vrEIhAajhM
//stage 2: https://www.youtube.com/watch?v=rWylZKi8QbM
//stage 3: https://www.youtube.com/watch?v=YgZSSE3qZqA
//stage 4: https://www.youtube.com/watch?v=Q7JKBDNIgvI
//stage 5: https://www.youtube.com/watch?v=0oR6sSgva-o
//stage 6: https://www.youtube.com/watch?v=Qs0MkIk-erA - GUI
#pragma endregion


#pragma region Main

// hIntance -> representation of the program
// hPrevInstance -> it can be removed
// lpCmdLine -> gives the ability to put differenent commands at runtime
// nCmdShow -> the command to show the window at start
//int CALLBACK WinMain(HINSTANCE, HINSTANCE, LPSTR, INT) 
//{

//	InitWindow();
//	//Create message listener
//	StartMessageLoop();
//
//	return 0;
//}


extern Win32::IApplication* EntryApplication(); //seems like 'extern' is not needed

INT CALLBACK WinMain(HINSTANCE, HINSTANCE, LPSTR, INT)
{
	auto EntryApp = EntryApplication();

	PerGameSettings gameSettings;

	EntryApp->SetGameSettings();

	CmdLineArgs::ReadArgs();

	Logger logger;

	EntryApp->PreInit();

	EntryApp->Init();

	MSG msg = { 0 };
	while (msg.message != WM_QUIT)
	{
		//if there are window messages then process them.
		//PeekMEssage is non blocking
		if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			EntryApp->Update();
		}
	}
	return 0;
}