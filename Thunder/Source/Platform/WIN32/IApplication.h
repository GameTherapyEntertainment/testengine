#pragma once

#define ENTRYAPP(x) Win32::IApplication* EntryApplication() { return new x; }

namespace Win32
{
	class THUNDER_API IApplication
	{
		//constructor
		public:
		IApplication();

		//deconstructor
		virtual ~IApplication()
		{
		};

		public:
		virtual VOID SetGameSettings() = 0;
		virtual void PreInit() = 0;
		virtual VOID Init() = 0;

		//Game loop
		virtual VOID Update() = 0;
	};

	IApplication* EntryApplication();
}