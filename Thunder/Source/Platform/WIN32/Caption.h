#pragma once

#define CB_CLOSE 0
#define CB_MINIMIZE 1
#define CB_MAXIMIZE 2

namespace Win32
{
	class Caption
	{
		public:
		struct CaptionBtn
		{
			wstring Text = L"X";

			int Command = -1;
			int Width = 50;
			RECT Rect;

			CaptionBtn(wstring text, int command, int width = 50)
			{
				Command = command;
				Text = text;
				Width = width;
			}
		};

		list<CaptionBtn*> m_CaptionBtns;

		private:
		bool m_ShowTitle = true;

		public:
		bool ShouldDrawTitle()
		{
			return m_ShowTitle;
		}
		void ShouldDrawTitle(bool show)
		{
			m_ShowTitle = show;
		}

		list<CaptionBtn*> CaptionBtns()
		{
			return m_CaptionBtns;
		}

		public:
		void AddCaptionBtn(CaptionBtn* btn);
	};
}

