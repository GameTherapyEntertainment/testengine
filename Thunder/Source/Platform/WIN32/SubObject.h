#pragma once

namespace Win32
{
	class  THUNDER_API SubObject
	{
		public:
		SubObject(std::wstring objName, std::wstring objDesc, HICON icon);
		~SubObject();

		VOID RegisterObject();
		LRESULT WindowProcess(HWND winProces, UINT message, WPARAM wparam, LPARAM lparam);

		virtual VOID Init() = 0;

		protected:
		std::wstring m_ObjName;
		std::wstring m_ObjDesc;
		HICON m_Icon;
		HWND m_Handle;

		protected:
		static			LRESULT CALLBACK	SetupMessageHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
		static			LRESULT				AssignMessageHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
		virtual			LRESULT				MessageHandler(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);

		public:
		HWND GetHandle()		{ return m_Handle; }
		void SetHandle(HWND hwnd)
		{
			m_Handle = hwnd;
		}
	};
}