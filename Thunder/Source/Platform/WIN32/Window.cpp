﻿#include "Thunder.h"
#include "Window.h"


#define DCX_USESTYLE 0x00010000

namespace Win32
{
	Window::Window(std::wstring title, HICON icon, WindowType type)
		: Win32::SubObject(title, title, icon), m_Type(type)
	{
		SetSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
	}

	Window::~Window()
	{
	}

	VOID Window::Init()
	{
		RECT desktop;
		const HWND hDesktop = GetDesktopWindow();
		GetWindowRect(hDesktop, &desktop);

		SIZE size = GetSize();
		RECT R = { 0, 0, size.cx, size.cy };

		AdjustWindowRect(&R, m_Type, false);
		int width = R.right - R.left;
		int height = R.bottom - R.top;

		m_Handle = CreateWindow(m_ObjName.c_str(), m_ObjDesc.c_str(),
								m_Type, ((desktop.right / 2) - (size.cx / 2)), ((desktop.bottom / 2) - (size.cy / 2)), size.cx, size.cy, 0, 0, HInstance(), (void*)this);

		ShowWindow(m_Handle, SW_SHOW);
		UpdateWindow(m_Handle);

	}

	LRESULT Window::MessageHandler(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
	{

		switch (message)
		{
			case WM_NCCREATE:
			{
				OnNonClientCreate();
			} return true;
			case WM_NCPAINT:
			{
				Window::OnNonClientPaint((HRGN)wParam);
			} return false;
			case WM_TIMER:
			{
				RedrawWindow();
			} break;
			case WM_NCACTIVATE:
			{
				OnNonClientActivate(LOWORD(wParam) != WA_INACTIVE);
			} return true;
			case WM_NCLBUTTONDOWN:
			{
				OnNonClientLeftMouseBtnDown();
			} break;
			case WM_GETMINMAXINFO:
			{
				OnGetMinMaxInfo((MINMAXINFO*)lParam);
			} return 0;
			case WM_NCLBUTTONDBLCLK:
			{
				Win32::Utils::MaximizeWindow(GetHandle());
			} return 0;
			case WM_EXITSIZEMOVE:
			{
				OnExitSizeMove();
			} break;
			case WM_PAINT:
			{
				OnPaint();
			} break;
		}

		return SubObject::MessageHandler(hwnd, message, wParam, lParam);
	}

	VOID Window::RedrawWindow()
	{
		SetWindowPos(GetHandle(), 0, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOACTIVATE | SWP_DRAWFRAME | SWP_FRAMECHANGED); // reset window
		SendMessage(GetHandle(), WM_PAINT, 0, 0);
	}

	void Window::OnNonClientCreate()
	{
		uintptr_t eventId = 1;
		uint32_t timeInMs = 100;
		SetTimer(GetHandle(), eventId, timeInMs, NULL);
		SetWindowTheme(GetHandle(), L"", L""); //reset theme

		Win32::Caption::AddCaptionBtn(new CaptionBtn(L"X", CB_CLOSE));
		Win32::Caption::AddCaptionBtn(new CaptionBtn(L"🗖", CB_MAXIMIZE));
		Win32::Caption::AddCaptionBtn(new CaptionBtn(L"_", CB_MINIMIZE));
	}

	void Window::OnNonClientActivate(bool active)
	{
		SetActive(active);
	}

	void Window::OnNonClientPaint(HRGN region)
	{
		//Start draw
		HWND hWnd = GetHandle();
		HDC hdc = GetDCEx(hWnd, region, DCX_WINDOW | DCX_INTERSECTRGN | DCX_USESTYLE);

		RECT rect;
		GetWindowRect(hWnd, &rect);

		SIZE size = SIZE{ rect.right - rect.left, rect.bottom - rect.top };

		HBITMAP bitmap = CreateCompatibleBitmap(hdc, size.cx, size.cy);
		HANDLE hOld = SelectObject(hdc, bitmap);

		//Draw
		HBRUSH brush = CreateSolidBrush(RGB(46, 46, 46));

		RECT newRect = RECT(0, 0, size.cx, size.cy);

		FillRect(hdc, &newRect, brush);

		if (IsActive() && !Win32::Utils::IsWindowFullscreen(GetHandle()))
		{
			brush = CreateSolidBrush(RGB(0, 150, 100));
			FrameRect(hdc, &newRect, brush);
		}

		PaintCaption(hdc);

		DeleteObject(brush);

		//End draw
		BitBlt(hdc, 0, 0, size.cx, size.cy, hdc, 0, 0, SRCCOPY);

		SelectObject(hdc, hOld);
		DeleteObject(bitmap);

		ReleaseDC(hWnd, hdc);
	}

	void Window::PaintCaption(HDC hdc)
	{
		RECT rect;
		GetWindowRect(GetHandle(), &rect);
		SIZE size = SIZE{ rect.right - rect.left, rect.bottom - rect.top };

		if (ShouldDrawTitle())
		{
			rect = RECT{ 0, 0, size.cx, 30 };

			SetBkMode(hdc, TRANSPARENT);
			SetTextColor(hdc, IsActive() ? RGB(255, 255, 255) : RGB(92, 92, 92));

			DrawText(hdc, m_ObjDesc.c_str(), wcslen(m_ObjDesc.c_str()), &rect, DT_SINGLELINE | DT_VCENTER | DT_CENTER);
		}

		int spacing = 0;
		POINT pt;
		GetCursorPos(&pt);

		GetWindowRect(GetHandle(), &rect);

		pt.x -= rect.left;
		pt.y -= rect.top;

		for (auto& btn : Caption::CaptionBtns())
		{
			spacing += btn->Width;
			btn->Rect = RECT{ size.cx - spacing, 0, size.cx - spacing + btn->Width, 30 };

			if (btn->Rect.left < pt.x && btn->Rect.right > pt.x && btn->Rect.top < pt.y && btn->Rect.bottom > pt.y)
			{

				/*switch (button->Command) {
					case CB_CLOSE: {	SendMessage(Handle(), WM_CLOSE, 0, 0); } break;
					case CB_MINIMIZE: { ShowWindow(Handle(), SW_MINIMIZE); } break;
					case CB_MAXIMIZE: { Win32::Utils::MaximizeWindow(Handle()); } break;
				}*/
				HBRUSH brush = CreateSolidBrush(RGB(92, 92, 92));
				FillRect(hdc, &btn->Rect, brush);
				DeleteObject(brush);

			}

			if (btn->Text.compare(L"🗖") == 0 && Win32::Utils::IsWindowFullscreen(GetHandle()))
			{
				btn->Text = L"🗗";
			}
			else if (btn->Text.compare(L"🗗") == 0 && !Win32::Utils::IsWindowFullscreen(GetHandle()))
			{
				btn->Text = L"🗖";
			}
			 
			DrawText(hdc, btn->Text.c_str(), wcslen(btn->Text.c_str()), &btn->Rect, DT_SINGLELINE | DT_VCENTER | DT_CENTER);
		}
	}

	void Window::OnNonClientLeftMouseBtnDown()
	{
		RECT rect;

		int spacing = 0;
		POINT pt;
		GetCursorPos(&pt);

		GetWindowRect(GetHandle(), &rect);

		pt.x -= rect.left;
		pt.y -= rect.top;

		for (auto& btn : Caption::CaptionBtns())
		{
			if (btn->Rect.left < pt.x && btn->Rect.right > pt.x && btn->Rect.top < pt.y && btn->Rect.bottom > pt.y)
			{

				switch (btn->Command)
				{
					case CB_CLOSE:
					{
						SendMessage(GetHandle(), WM_CLOSE, 0, 0);
					} break;
					case CB_MINIMIZE:
					{
						ShowWindow(GetHandle(), SW_MINIMIZE);
					} break;
					case CB_MAXIMIZE:
					{
						Win32::Utils::MaximizeWindow(GetHandle());
					} break;
				}
			}

		}
	}

	//minmax will be supplied by the system
	void Window::OnGetMinMaxInfo(MINMAXINFO* minmax)
	{
		RECT WorkArea;
		SystemParametersInfo(SPI_GETWORKAREA, 0, &WorkArea, 0);
		minmax->ptMaxSize.x = (WorkArea.right - WorkArea.left);
		minmax->ptMaxSize.y = (WorkArea.bottom - WorkArea.top);
		minmax->ptMaxPosition.x = WorkArea.left;
		minmax->ptMaxPosition.y = WorkArea.top;
		minmax->ptMinTrackSize.x = 400;
		minmax->ptMinTrackSize.y = 300;
	}

	void Window::OnExitSizeMove() {
		RECT rect;
		GetWindowRect(GetHandle(), &rect);
		RECT WorkArea; 
		SystemParametersInfo(SPI_GETWORKAREA, 0, &WorkArea, 0);
		if (rect.top < WorkArea.top + 10 && !Win32::Utils::IsWindowFullscreen(GetHandle()))
			Win32::Utils::MaximizeWindow(GetHandle());
	}

	void Window::OnPaint() {
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(GetHandle(), &ps);

		RECT rc;
		GetClientRect(GetHandle(), &rc);

		HBRUSH brush = CreateSolidBrush(RGB(36, 36, 36));

		FillRect(hdc, &rc, brush);

		DeleteObject(brush);

		EndPaint(GetHandle(), &ps);

	}
}

