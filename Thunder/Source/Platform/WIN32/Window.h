#pragma once
#include "SubObject.h"

#include <Uxtheme.h>
#pragma comment(lib,"uxtheme.lib")

namespace Win32
{
	class  THUNDER_API Window : public Win32::SubObject, public Win32::Caption
	{
		public:
		Window(std::wstring title, HICON icon, WindowType type = RESIZABLE);
		~Window();

		virtual VOID Init() override;

		virtual			LRESULT				MessageHandler(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) override;

		VOID RedrawWindow();

		void OnNonClientCreate();

		void OnNonClientActivate(bool active);

		void OnNonClientPaint(HRGN region);

		void PaintCaption(HDC hdc);

		void OnNonClientLeftMouseBtnDown();

		void OnGetMinMaxInfo(MINMAXINFO* minmax);

		void OnExitSizeMove();

		void OnPaint();

		protected:
		SIZE m_Size;
		WindowType m_Type;

		bool m_Active;

		public:
		SIZE GetSize()
		{
			return m_Size;
		}

		VOID SetSize(SIZE size)
		{
			m_Size = size;
		}

		VOID SetSize(int cx, int cy)
		{
			m_Size.cx = cx;
			m_Size.cy = cy;
		}

		bool IsActive()
		{
			return m_Active;
		}

		void SetActive(bool active)
		{
			m_Active = active;
		}
	};
}