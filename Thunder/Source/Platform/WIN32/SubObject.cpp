#include "Thunder.h"
#include "SubObject.h"

namespace Win32
{
	SubObject::SubObject(std::wstring objName, std::wstring objDesc, HICON icon)
		: m_ObjName(objName), m_ObjDesc(objDesc), m_Icon(icon)
	{

	}

	SubObject::~SubObject()
	{
	}

	VOID SubObject::RegisterObject()
	{
		//Init Window class
		WNDCLASSEX window{};
		//set reference to an instance of the program
		window.hInstance = HInstance();

		window.cbSize = sizeof(WNDCLASSEX);
		window.style = CS_HREDRAW | CS_VREDRAW;
		//add extra memory at runtime, this could be done later
		window.cbClsExtra = 0;
		window.cbWndExtra = 0;

		//set Window properties as cursor and background etc.
		window.hCursor = LoadCursor(nullptr, IDC_ARROW);
		window.hbrBackground = (HBRUSH)(CreateSolidBrush(RGB(36, 36, 36)));
		window.hIcon = m_Icon;
		window.hIconSm = m_Icon;

		window.lpszClassName = m_ObjName.c_str();
		window.lpszMenuName = nullptr;

		//use the default windows process which sets instructions how the window is going to perform
		//could set specific commands later
		window.lpfnWndProc = SetupMessageHandler;

		//* -> pointer
		//& -> reference
		//https://www.educba.com/c-plus-plus-pointer-vs-reference/
		RegisterClassEx(&window);
	}

	LRESULT SubObject::SetupMessageHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		if (msg == WM_NCCREATE)
		{
			const CREATESTRUCTW* const pCreate = reinterpret_cast<CREATESTRUCTW*>(lParam);
			Win32::SubObject* const pWnd = static_cast<Win32::SubObject*>(pCreate->lpCreateParams);
			SetWindowLongPtr(hWnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(pWnd));
			SetWindowLongPtr(hWnd, GWLP_WNDPROC, reinterpret_cast<LONG_PTR>(&Win32::SubObject::AssignMessageHandler));
			pWnd->SetHandle(hWnd);
			return pWnd->MessageHandler(hWnd, msg, wParam, lParam);
		}
		return DefWindowProc(hWnd, msg, wParam, lParam);
	}

	LRESULT SubObject::AssignMessageHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		Win32::SubObject* const pWnd = reinterpret_cast<Win32::SubObject*>(GetWindowLongPtr(hWnd, GWLP_USERDATA));
		return pWnd->MessageHandler(hWnd, msg, wParam, lParam);
	}

	LRESULT SubObject::MessageHandler(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
	{
		return DefWindowProc(hwnd, message, wParam, lParam);
	}

	//HWND -> instance of the window that`s running
	//UINT -> message type(instructions) e.g. close window
	//WPARAM -> argument for the UINT
	//LPARAM -> argument for the UINT
	//LRESULT CALLBACK SubObject::WindowProcess(HWND winProces, UINT message, WPARAM wparam, LPARAM lparam)
	//{
	//	switch (message)
	//	{
	//		case WM_DESTROY:
	//		PostQuitMessage(0);
	//		break;
	//	}
	//	return DefWindowProc(winProces, message, wparam, lparam);
	//}
}
